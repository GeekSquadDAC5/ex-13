﻿using System;

namespace ex_13
{
    class Program
    {
        static void Main(string[] args)
        {
            // --------------------------------------------
            // Exercise #13 - Creating a simple app with a single variable
            // --------------------------------------------
            // Start the program with Clear();
            Console.Clear();
            
            
            // Print out my name
            var myName = "Sing Graajae Cho (Shinkee Cho)";
            Console.WriteLine("Welcome to NewShop");
            Console.WriteLine($"My name is {myName}.");
            Console.WriteLine("Hello World!");
        }
    }
}
